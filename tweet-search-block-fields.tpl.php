<?php
/**
 * @file
 * Format a tweet from tweet_search module
 *
 * @varibles
 * $tweet : The tweet array
 *   $tweet['profile_image'] : The user image
 *   $tweet['from_user'] : The user name
 *   $tweet['text'] : The tweet text
 *   $tweet['created_at'] : The tweet date created
 */
?>

<div class="tweet-search-img"><?php print $tweet['profile_image']; ?></div>
<div class="tweet-search-user"><h4><?php print $tweet['from_user']; ?></h4></div>
<div class="tweet-search-text"><?php print $tweet['text']; ?></div>
<div class="tweet-search-date"><?php print $tweet['created_at']; ?></div>