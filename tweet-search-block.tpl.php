<?php
/**
 * @file
 * Display tweets from tweet_search module
 *
 * @varibles
 * $row : An array of formatted tweets. @see tweet_search_block_fields.tpl.php
 *
 * $tweets : The full array for advanced use
 */
?>

<ul>
  <?php
    foreach ($rows as $row) {
    	print '<li>' . $row . '</li>';
    }
  ?>
</ul>