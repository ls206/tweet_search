<?php
/**
 * @file
 * Admin tools for the Tweet search module.
 */

function tweet_search_admin_form($form, &$form_state) {
  $form = array();
  if (isset($form_state['storage']['step'])) {
    return tweet_search_add_new_form();
  }

  $searches = variable_get('tweet_search_blocks', array());

  $form['#validate'] = array('tweet_search_admin_form_validate');

  $form['tabs'] = array(
    '#type' => 'vertical_tabs',
  );

  foreach ($searches as $key => $block) {
    $form[$key] = array(
      '#type' => 'fieldset',
      '#title' => filter_xss($block['name']),
      '#description' => 'machine name: ' . filter_xss($block['block_name']),
      '#collapsible' => TRUE,
      '#tree' => TRUE,
      '#group' => 'tabs',
    );
    $form[$key]['name'] = array(
      '#type' => 'hidden',
      '#value' => $block['name'],
    );
    $form[$key]['block_name'] = array(
      '#type' => 'hidden',
      '#value' => $block['block_name'],
    );
    $form[$key]['title'] = array(
      '#type' => 'textfield',
      '#title' => 'Title',
      '#default_value' => $block['title'],
    );
    $form[$key]['search'] = array(
      '#type' => 'textfield',
      '#title' => 'Search query',
      '#default_value' => urldecode($block['search']),
      '#description' => t('<p>See ' . l(t('Twitter search'), 'http://twitter.com/#!/search-home', array('attributes' => array('target' => '_blank'))) . ' for search tests and operators, or ' . l(t('Twitter search operators'), 'admin/help/tweet_search') . '</p>'),
    );
    $form[$key]['advanced'] = array(
      '#type' => 'fieldset',
      '#title' => t('advanced'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#tree' => TRUE,
    );
    $form[$key]['advanced']['cache_life'] = array(
      '#type' => 'textfield',
      '#title' => 'Cache lifetime',
      '#default_value' => $block['cache_life'],
      '#description' => '<p>Enter a time in seconds for the minimum cache lifetime of the tweets, e.g. 60 = 1 minute. Set as 0 for no additional caching<br /><strong>NOTE:</strong> due to the limitations of the twitter search api setting this value to 0 may result in a rate limit being enforced<br />Clear the cache after changing this value</p>',
    );
    $form[$key]['delete'] = array(
      '#type' => 'checkbox',
      '#title' => 'Delete',
      '#description' => 'Remove this block and search query completely',
      '#default_value' => FALSE,
    );
  }
  if (!empty($searches)) {
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Save Settings',
      '#submit' => array('tweet_search_admin_form_submit'),
    );
  }
  $form['add_new'] = array(
    '#type' => 'button',
    '#value' => 'Add new search block',
    '#validate' => array('tweet_search_add_new'),
  );

  return $form;
}

function tweet_search_admin_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  foreach ($values as $key => $value) {
    if (!$value['delete']) {
      if (preg_match('/^[0-9]+$/', $value['advanced']['cache_life']) < 1) {
        form_set_error($key . '][advanced][cache_life', 'The cache lifetime must consist of only numbers');
      }
      if (trim($value['search']) == '') {
        form_set_error($key . '][search', 'The search query cannot be empty');
      }
    }
  }
}

function tweet_search_admin_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  if (isset($form_state['storage']['step'])) {
    // process the new search block
    $original = variable_get('tweet_search_blocks', array());
    $new_search = array(
      'name' => check_plain($values['name']),
      'block_name' => check_plain($values['block_name']),
      'title' => check_plain($values['title']),
      'search' => urlencode($values['search']),
      'cache_life' => $values['advanced']['cache_life'],
    );
    $original[$values['block_name']] = $new_search;
    variable_set('tweet_search_blocks', $original);
    $msg = sprintf('The new twitter search has been added. %s', l(t('Configure blocks'), 'admin/structure/block'));
    drupal_set_message(t($msg));
    unset($form_state['storage']);
  }
  else {
    // save the changed fields
    $save_arr = array();
    $clear_block_cache = 0;
    foreach ($values as $key => $value) {
      // CHECK IF DELETE FLAG IS SET
      if (is_array($value)) {
        $delete = $value['delete'];
        if (!$delete) {
          $save_arr[$value['block_name']] = array(
            'name' => check_plain($value['name']),
            'block_name' => check_plain($value['block_name']),
            'title' => check_plain($value['title']),
            'search' => urlencode($value['search']),
            'cache_life' => $value['advanced']['cache_life'],
          );
        }
        else {
          // delete block from db
          db_delete('block')
            ->condition(
              db_and()
              ->condition('module', 'tweet_search')
              ->condition('delta', $value['block_name'])
            )
            ->execute();
          $clear_block_cache = 1;
        }
      }
    }
    if ($clear_block_cache) {
      cache_clear_all(NULL, 'cache_block');
    }
    variable_set('tweet_search_blocks', $save_arr);
    drupal_set_message(t('The changes have been saved'));
  }
}


function tweet_search_add_new($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  $form_state['storage']['step'] = 1;
}

function tweet_search_add_new_validate($form, &$form_state) {
  $values = $form_state['values'];
  if (trim($values['name']) == '') {
    form_set_error('name', 'The name of the search block cannot be empty');
  }
  if (trim($values['search']) == '') {
    form_set_error('search', 'The search query cannot be empty');
  }
  if (preg_match('/^[0-9]+$/', $values['advanced']['cache_life']) < 1) {
    form_set_error('advanced][cache_life', 'The cache lifetime must consist of only numbers');
  }
}

function tweet_search_add_new_form() {
  $form_new = array();

  drupal_set_title(t('Add a new twitter search block'));

  $form_new['name'] = array(
    '#type' => 'textfield',
    '#title' => 'Name',
  );
  $form_new['block_name'] = array(
    '#type' => 'machine_name',
    '#machine_name' => array(
      'exists' => 'tweet_search_name_exists',
    ),
  );
  $form_new['title'] = array(
    '#type' => 'textfield',
    '#title' => 'Title',
  );
  $form_new['search'] = array(
    '#type' => 'textfield',
    '#title' => 'Search query',
    '#description' => t('<p>See ' . l(t('Twitter search'), 'http://twitter.com/#!/search-home', array('attributes' => array('target' => '_blank'))) . ' for search tests and operators, or ' . l(t('Twitter search operators'), 'admin/help/tweet_search') . '</p>'),
  );
  $form_new['advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('advanced'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#tree' => TRUE,
  );
  $form_new['advanced']['cache_life'] = array(
    '#type' => 'textfield',
    '#title' => 'Cache lifetime',
    '#default_value' => '300',
    '#description' => '<p>Enter a time in seconds for the minimum cache lifetime of the tweets, e.g. 60 = 1 minute. Set as 0 for no additional caching<br /><strong>NOTE:</strong> due to the limitations of the twitter search api setting this value to 0 may result in a rate limit being enforced</p>',
  );
  $form_new['save'] = array(
    '#type' => 'submit',
    '#value' => 'save settings',
    '#submit' => array('tweet_search_admin_form_submit'),
  );
  $form_new['#validate'] = array('tweet_search_add_new_validate');
  return $form_new;
}

function tweet_search_name_exists($value) {
  $stored = variable_get('tweet_search_blocks', array());
  foreach ($stored as $key => $arr) {
    if ($value == $key) {
      return TRUE;
    }
  }
  return FALSE;
}